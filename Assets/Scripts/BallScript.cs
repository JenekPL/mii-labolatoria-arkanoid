﻿using UnityEngine;
using System.Collections;

public class BallScript : MonoBehaviour {

	public int lives = 3;
	public int score = 0;

	public void startPosition(){
		GetComponent<Rigidbody>().Sleep();
		transform.position = new Vector3(0.0f,0.45f,0.0f);
		
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision col) {
		if (col.transform.name == "Block"){ 
			Destroy(col.gameObject);
			score++;
			Debug.Log (score);
		}
	}
}
