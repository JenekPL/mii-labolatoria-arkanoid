﻿using UnityEngine;
using System.Collections;

public class PaddleController : MonoBehaviour {

	public float paddleMoveSpeed = 0.1f;
	public float paddleRotateSpeed = 1.0f;


	public void startPosition(){
		transform.position = new Vector3(0.0f,0.0f,0.0f);
		transform.eulerAngles = new Vector3(0.0f,0.0f,0.0f);
		
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	if(Input.GetKey(KeyCode.LeftArrow)) {
			transform.Translate(new Vector3(-paddleMoveSpeed, 0.0f, 0.0f));
			Debug.Log("Left");
	}

	if(Input.GetKey(KeyCode.RightArrow)) {
		transform.Translate(new Vector3(paddleMoveSpeed, 0.0f, 0.0f));
			Debug.Log("Right");
	}

	if(Input.GetKey(KeyCode.UpArrow)) {
			transform.Rotate(new Vector3(0.0f,0.0f,paddleRotateSpeed));
			Debug.Log("Up");
	}

	if(Input.GetKey(KeyCode.DownArrow)) {
			transform.Rotate(new Vector3(0.0f,0.0f,-paddleRotateSpeed));
			Debug.Log("Down");
		}
}
}