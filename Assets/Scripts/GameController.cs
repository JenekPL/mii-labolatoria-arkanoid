﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public BallScript bs;
	public PaddleController pc;
	public Text textLives;
	public Text textScore;

	public bool stopped;

	public void Reset(){
		stopped = true;
		bs.startPosition();
		pc.startPosition();
	}

	// Use this for initialization
	void Start () {
		setTexts();
		Reset();
	}
	
	// Update is called once per frame
	void Update () {
		setTexts();
		if(stopped) {
			if(Input.GetKey(KeyCode.Space) && bs.lives > 0){
				stopped = false;
				bs.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0.0f, -400, 0.0f));
			}
		}
	}

	void setTexts(){
		textLives.text = bs.lives.ToString();
		textScore.text = bs.score.ToString();
	}

}
